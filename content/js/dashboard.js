/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.9155168999207817, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.15584415584415584, 500, 1500, "getCountCheckInOutChildren"], "isController": false}, {"data": [0.9994352834876892, 500, 1500, "getLatestMobileVersion"], "isController": false}, {"data": [0.7824471126357919, 500, 1500, "findAllConfigByCategory"], "isController": false}, {"data": [0.7591954022988506, 500, 1500, "getNotifications"], "isController": false}, {"data": [0.0779467680608365, 500, 1500, "getHomefeed"], "isController": false}, {"data": [0.7541924095322153, 500, 1500, "me"], "isController": false}, {"data": [0.7866161616161617, 500, 1500, "findAllChildrenByParent"], "isController": false}, {"data": [0.7108895705521472, 500, 1500, "getAllClassInfo"], "isController": false}, {"data": [0.7622594661700807, 500, 1500, "findAllSchoolConfig"], "isController": false}, {"data": [0.0, 500, 1500, "getChildCheckInCheckOut"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 30296, 0, 0.0, 496.8957948244015, 10, 12128, 68.0, 441.0, 3081.9000000000015, 8625.960000000006, 100.00165041012691, 256.78440731193575, 101.01910246571273], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getCountCheckInOutChildren", 539, 0, 0.0, 2792.2337662337645, 416, 9060, 2100.0, 5999.0, 6631.0, 8152.600000000004, 1.7792184642604856, 1.124174166383334, 2.20143534591605], "isController": false}, {"data": ["getLatestMobileVersion", 22135, 0, 0.0, 67.30616670431475, 10, 1117, 49.0, 133.0, 171.0, 263.0, 73.79464851277196, 49.364584210203894, 54.409140260881664], "isController": false}, {"data": ["findAllConfigByCategory", 1749, 0, 0.0, 859.2201257861627, 34, 8186, 202.0, 2900.0, 4672.5, 6947.0, 5.783538904136767, 6.540369190420289, 7.511822990724513], "isController": false}, {"data": ["getNotifications", 870, 0, 0.0, 1735.0287356321833, 74, 9384, 285.0, 8257.1, 8543.35, 8916.58, 2.874218262188172, 23.89474615821085, 3.197006445930008], "isController": false}, {"data": ["getHomefeed", 263, 0, 0.0, 5740.3916349809915, 952, 12128, 6762.0, 9413.8, 9651.2, 10950.920000000004, 0.8688901002692568, 10.587052520565935, 4.3478446033004605], "isController": false}, {"data": ["me", 1133, 0, 0.0, 1330.624007060901, 57, 8705, 255.0, 5338.2, 7487.099999999998, 8406.060000000003, 3.7434126839905506, 4.926829038499661, 11.862670077684898], "isController": false}, {"data": ["findAllChildrenByParent", 1188, 0, 0.0, 1270.5597643097653, 51, 8793, 231.0, 5938.600000000005, 7819.15, 8559.11, 3.9249890972525074, 4.4079467400785, 6.270783362407327], "isController": false}, {"data": ["getAllClassInfo", 652, 0, 0.0, 2311.995398773007, 75, 10006, 367.0, 8606.7, 8873.35, 9266.810000000001, 2.1545385751626647, 6.182877846626263, 5.531525306740863], "isController": false}, {"data": ["findAllSchoolConfig", 1611, 0, 0.0, 932.9354438237096, 42, 8482, 239.0, 3026.3999999999996, 4981.199999999982, 7316.879999999985, 5.338573596760403, 116.42678277622396, 3.915301534342835], "isController": false}, {"data": ["getChildCheckInCheckOut", 156, 0, 0.0, 9677.839743589746, 5929, 11254, 9687.0, 10186.900000000001, 10726.0, 11114.350000000002, 0.5152613134539352, 34.35494743756585, 2.371007137690374], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 30296, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
